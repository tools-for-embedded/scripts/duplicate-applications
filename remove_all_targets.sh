#!/usr/bin/env bash

#------------------------------------------------------------------------
# Author:   Laurent von Allmen                                 2023-12-18
# Modifs:
#
# Goal:     Erase all targets created by script create_all_targets.sh.
#
#           usage:
#           ./remove_all_targets.sh
#
#   (c) 1992-2023, Laurent von Allmen
#   ---------------------------------
#
#   CSEM S.A.
#   Jaquet-Droz 1
#   CH-2000 Neuchâtel
#   http://www.csem.ch
#
#   ____________________/\\\______/\\\______/\\\_
#    ________________/\\\\\\\__/\\\\\\\__/\\\\\\\_
#     _______________\/////\\\_\/////\\\_\/////\\\_
#      __/\\\____/\\\_____\/\\\_____\/\\\_____\/\\\_
#       _\/\\\___\/\\\_____\/\\\_____\/\\\_____\/\\\_
#        _\//\\\\\\\\\______\/\\\_____\/\\\_____\/\\\_
#         __\/////////_______\///______\///______\///_
#
#   THIS PROGRAM IS CONFIDENTIAL AND CANNOT BE DISTRIBUTED
#   WITHOUT THE CSEM PRIOR WRITTEN AGREEMENT.
#
#   u111 is an optimised branch of uKOS-III package.
#   CSEM is the owner of this branch and is authorised to use, to modify
#   and to keep confidential all new adaptations of this branch.
#------------------------------------------------------------------------

if [ -z "$PATH_UKOS_KERNEL" ]; then
    echo "Variable PATH_UKOS_KERNEL is not set!"
    exit 1
fi

find $PATH_UKOS_KERNEL/Applications/uKOS_Appls_Downloadable -type d -name "Edo_NewDevil" -exec rm -fr {} \;
find $PATH_UKOS_KERNEL/Applications/uKOS_Appls_Downloadable -type d -name "Black_Angel" -exec rm -fr {} \; 
