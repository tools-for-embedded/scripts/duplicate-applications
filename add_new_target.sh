#!/usr/bin/env bash

#------------------------------------------------------------------------
# Author:   Laurent von Allmen                                 2023-12-18
# Modifs:
#
# Goal:     Creating all targets for downloadable applications.
#
#           usage:
#           ./add_new_target.sh Target SOC Core [refTarget]
#
#   (c) 1992-2023, Laurent von Allmen
#   ---------------------------------
#
#   CSEM S.A.
#   Jaquet-Droz 1
#   CH-2000 Neuchâtel
#   http://www.csem.ch
#
#   ____________________/\\\______/\\\______/\\\_
#    ________________/\\\\\\\__/\\\\\\\__/\\\\\\\_
#     _______________\/////\\\_\/////\\\_\/////\\\_
#      __/\\\____/\\\_____\/\\\_____\/\\\_____\/\\\_
#       _\/\\\___\/\\\_____\/\\\_____\/\\\_____\/\\\_
#        _\//\\\\\\\\\______\/\\\_____\/\\\_____\/\\\_
#         __\/////////_______\///______\///______\///_
#
#   THIS PROGRAM IS CONFIDENTIAL AND CANNOT BE DISTRIBUTED
#   WITHOUT THE CSEM PRIOR WRITTEN AGREEMENT.
#
#   u111 is an optimised branch of uKOS-III package.
#   CSEM is the owner of this branch and is authorised to use, to modify
#   and to keep confidential all new adaptations of this branch.
#------------------------------------------------------------------------

# Save parameters
export new_target=$1
export soc_new_target=$2
export core_new_target=$3
# check for optional reference target to use as source for copy
if [ $# -gt 3 ]; then
	export reference_target=$4
fi

# Create a list of all folders in uKOS_Appls_Downloadable
root_dir=$PATH_UKOS_KERNEL/Applications/uKOS_Appls_Downloadable
cd $root_dir
app_family_list=`find . -type d -depth 1`


for app_family in $app_family_list
do
	# Create a list of all applications in current folder
	cd $root_dir/$app_family
	app_list=`find . -type d -depth 1`
	for app in $app_list
	do
		if [ -z "$reference_target" ]; then
			echo "Using first target found as reference"
			# Find first target and use it as reference
			cd $root_dir/$app_family/$app
			target_list=`find . -type d -depth 1`
			set -- $target_list
			reference_target=$1
		fi
		rm -fr $new_target
		cp -r $reference_target $new_target
		cd $new_target
		mv makefile makefile.bak
		sed -e 's/BOARD\(.*\)=  \(.*\)/BOARD\1=  $new_target/' \
			-e 's/SOC\(.*\)=  \(.*\)/SOC\1=  $soc_new_target/' \
			-e 's/CORE\(.*\)=  \(.*\)/CORE\1=  $core_new_target/' \
			makefile.bak | envsubst '$new_target $soc_new_target $core_new_target' > makefile
		rm makefile.bak
	done
done
