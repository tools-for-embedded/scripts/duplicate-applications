#!/usr/bin/env bash

#------------------------------------------------------------------------
# Author:   Laurent von Allmen                                 2023-12-18
# Modifs:
#
# Goal:     Creating all targets for downloadable applications.
#
#           usage:
#           ./create_all_targets.sh
#
#   (c) 1992-2023, Laurent von Allmen
#   ---------------------------------
#
#   CSEM S.A.
#   Jaquet-Droz 1
#   CH-2000 Neuchâtel
#   http://www.csem.ch
#
#   ____________________/\\\______/\\\______/\\\_
#    ________________/\\\\\\\__/\\\\\\\__/\\\\\\\_
#     _______________\/////\\\_\/////\\\_\/////\\\_
#      __/\\\____/\\\_____\/\\\_____\/\\\_____\/\\\_
#       _\/\\\___\/\\\_____\/\\\_____\/\\\_____\/\\\_
#        _\//\\\\\\\\\______\/\\\_____\/\\\_____\/\\\_
#         __\/////////_______\///______\///______\///_
#
#   THIS PROGRAM IS CONFIDENTIAL AND CANNOT BE DISTRIBUTED
#   WITHOUT THE CSEM PRIOR WRITTEN AGREEMENT.
#
#   u111 is an optimised branch of uKOS-III package.
#   CSEM is the owner of this branch and is authorised to use, to modify
#   and to keep confidential all new adaptations of this branch.
#------------------------------------------------------------------------

if [ -z "$PATH_UKOS_KERNEL" ]; then
    echo "Variable PATH_UKOS_KERNEL is not set!"
    exit 1
fi

TAB=(

#   Target              SOC                     Core

    "Edo_NewDevil"      "STM32Z007"           "Cortex-A5"
    "Black_Angel"       "STM32OSS1117"        "Cortex-R4"
    )

SIZE_TAB=${#TAB[@]}
let "structure = 3"
let "nbElements = SIZE_TAB/structure"

echo "Building all ..."
for (( h = 0 ; h < nbElements ; h++ ))
do
    let "target = h * structure"
    let "soc = target + 1"
    let "core = target + 2"
    
    ./add_new_target.sh ${TAB[$target]} ${TAB[$soc]} ${TAB[$core]}
done

echo "Terminated!"
